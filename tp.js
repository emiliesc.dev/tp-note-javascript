var error = []
$('#submit').click(function (){
    verifyUsername();
    verifyPassword();
    verifyConfirmPassword();
    verifyMail();
    verifyTel();
    $('#liste').append($(`<div><ul><li>${error}</li></ul></div>`))
})

function verifyUsername(){
    var username = $('#username').val()
    var  usernameLenght = username.length
    if (usernameLenght < 8) {
        error.push('Identifiant trop court');
    } else if (!usernameLenght >= 8) {
        error.push('Identifiant obligatoire')
    }
}

function verifyPassword(){
    var password = $('#password').val()
    var  passwordLenght = password.length

    if (passwordLenght <8) {
        error.push('Mot de passe trop court');
    } else if (passwordLenght >= 8) {
        if (!password.match(/\d+/g)) {
            error.push('Le mot de passe doit contenir au moins un chiffre')
        }
        else if (password.match(/[\$,\^,\%,\@]/g)) {
            error.push('Le mot de passe doit contenir un caractère spécial')
        }
    }
}

function verifyConfirmPassword(){
    var password = $('#password').val()
    var passwordConfirm = $('#confirm-password').val()
    if (password !== passwordConfirm) {
        error.push('les mdp sont incorrects, ils ne sont pas identiques')
    } else  {
        error.push('les mdp sont identiques')
    }
}

function verifyTel(){
    var tel = $('#tel').val()
    var  telLenght = tel.length
    if (!(isNumeric(tel))) {
        error.push('le téléphone est invalide !')
    }
    if (telLenght !== 10) {
        error.push('Le téléphone est invalide !')
    }
}

function verifyMail(){

}