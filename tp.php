<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../jquery/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="styletp.css">
    <title>TP Emilie Sainte-croix</title>
</head>
<body>
<h1>Formulaire d'inscription</h1>
<div id="liste"></div>
<label for="username" >Username :</label>
<input type="text" name="username" id="username">

<label for="password" >Password :</label>
<input type="password" name="password" id="password">

<label for="confirm-password" >Confirm Password :</label>
<input type="password" name="confirm-password" id="confirm-password">

<label for="tel" >Telephone number :</label>
<input type="text" name="tel" id="tel">

<label for="mail" >Mail adress :</label>
<input type="text" name="mail" id="mail">

<input type="submit" id="submit" value="Envoyer le formulaire">

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script src="../jquery/jquery-ui/external/jquery/jquery.js"></script>
<script src="../jquery/jquery-ui/jquery-ui.js"></script>
<script src="tp.js"></script>
</body>
</html>